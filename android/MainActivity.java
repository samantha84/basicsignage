package com.example.screendisplay;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Context;


public class MainActivity extends AppCompatActivity {

    //delcare vars
    String serveraddress;
    String screennumber;
    String url;
    //declare textfields and buttons etc
    //declare textfields and buttons etc
    EditText serveraddressinput;
    EditText screennumberinput;
    Button submitbutton;

    boolean urlcheck = false;
    SharedPreferences sharedpreferences;
    public static final String savedvalues = "mykey";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        serveraddressinput = (EditText) findViewById(R.id.serveraddressinput);
        screennumberinput = (EditText) findViewById(R.id.screennumberinput);
        submitbutton = (Button) findViewById(R.id.submitbutton);
        sharedpreferences = getSharedPreferences(savedvalues,Context.MODE_PRIVATE);
        hideSystemUI();



        if (sharedpreferences.contains(savedvalues)) {
            url=sharedpreferences.getString(savedvalues,"");

            // open webpage
            setContentView(R.layout.web_viewer);
            WebView myWebView = (WebView) findViewById(R.id.myWebView);
            myWebView.getSettings().setJavaScriptEnabled(true);
            myWebView.loadUrl(url);

            reloadWebView();
        }
        else{
            showToast("no saved data found");
        }



        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serveraddress = serveraddressinput.getText().toString(); //get value of input box and store in the serveraddresse var

                if (serveraddress.length() > 7) {
                   String serveraddresssnip = serveraddress.substring(0, 4);
                    if (serveraddresssnip.equals("http") || serveraddresssnip.equals("HTTP")  ) {

                        String lastchar= serveraddress.substring(serveraddress.length()-1);
                        if (lastchar != "/"){
                            serveraddress = serveraddress + "/";
                        }
                        screennumber = screennumberinput.getText().toString();
                        url = serveraddress + "screens/" + screennumber + ".html";
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(savedvalues, url);
                        editor.commit();
                        url = sharedpreferences.getString(savedvalues, url);
                        showToast(url);
                        setContentView(R.layout.web_viewer);

                        WebView myWebView = (WebView) findViewById(R.id.myWebView);
                        myWebView.getSettings().setAppCacheEnabled(false);
                        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                        myWebView.getSettings().setJavaScriptEnabled(true);
                        myWebView.loadUrl(url);


                    }

                    else{
                        showToast("invalid URL");
                    }
                }
                else{
                    showToast("invalid URL");
                }

            }
        });
    }
    private void showToast (String text){
        Toast.makeText(MainActivity.this,text,Toast.LENGTH_SHORT).show();
    }
    Handler handler = new Handler();
    public void reloadWebView() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast("reloading");
                WebView myWebView = (WebView) findViewById(R.id.myWebView);
                myWebView.getSettings().setAppCacheEnabled(false);
                myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                myWebView.getSettings().setJavaScriptEnabled(true);
                myWebView.reload();
                reloadWebView();
            }
        }, 300000);}

    private void hideSystemUI() {
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
}