**Basic (digital) Signage**

A few years ago some android clients were purchased for use with Xibo, however these quickly fell out of use as staff found Xibo too difficult and time consuming to use. They essentially just wanted to play very simple slideshows on the screens.

My solution was to make a webserver, with python running for the backend, to enable staff to upload images to a selected screen. The android clients would then use an app that utilizes WebView to display the resulting page.

When the android app is first run it promtps for the URL of the page which could be something like myserver.com/screens/01.html

A limitation I faced is the android clients we have are running android 4, as such I had to limit my javascript to things which are now legacy.